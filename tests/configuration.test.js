/* eslint-disable no-undef */
const Configuration = require('../lib/configuration.js');

describe('configuration.js', () => {
    describe('createOption', () => {
        it('add onto options object', () => {
            const config = new Configuration(null, {});

            config.createOption('key', 'value');

            expect(config.options).toEqual({
                key: 'value',
                presets: {}
            });
        });
    });

    describe('updateConfigurationFile', () => {
        it('calls writeFileSync with valid filepath and options', () => {
            const fakeJsonfile = {
                writeFileSync: jest.fn()
            };

            const config = new Configuration('path/to/file', {}, { jsonfile: fakeJsonfile });
            config.updateConfigurationFile();

            const writeFileSyncCalls = fakeJsonfile.writeFileSync.mock.calls[0];
            expect(writeFileSyncCalls[0]).toBe('path/to/file');
            expect(writeFileSyncCalls[1]).toEqual({ presets: {} });
        });
    });

    describe('getOption', () => {
        it('gets the value of a key', () => {
            const fakeJsonfile = {
                readFileSync: jest.fn().mockReturnValue({
                    'key1': '#',
                    'key2': '##',
                    'key3': '###'
                })
            };

            const config = new Configuration('path/to/file', {}, { jsonfile: fakeJsonfile });

            expect(config.getOption('key2')).toBe('##');
        });
    });
});