class Input {
    constructor({ prompts } = {}) {
        this.prompts = prompts || require('prompts');
    }

    async selectPreset(presets) {
        return (await this.prompts({
            type: 'select',
            name: 'preset',
            message: 'Select a preset from this list',
            choices: Object.entries(presets).map((preset, id) => ({ title: `${id + 1}. ${preset[0]}`, value: preset })),
            initial: 0
        })).preset;
    }

    async confirmOverwrite(file) {
        return (await this.prompts({
            type: 'confirm',
            name: 'confirmation',
            message: `Do you want to overwrite ${file}?`,
            initial: false
        })).confirmation;
    }
}

module.exports = Input;