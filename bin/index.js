#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const Arguments = require('../lib/arguments.js');
const Input = require('../lib/prompts.js');
const Generate = require('../lib/generation.js');
const Configuration = require('../lib/configuration.js');

// The old presets file  v0.1.0) that was required to be manually made in order for the presets functionality to work
const presetsFile = [ // Files lower in the array will take precedence, assuming they exist
    path.join(process.env.HOME, '.boiler-gen-presets'),
    path.join(process.cwd(), '.boiler-gen-presets')
].reduce((presetConfig, candidate) => (fs.existsSync(candidate) ? candidate : presetConfig), '');

const config = new Configuration(path.join(process.env.HOME, '.boiler-gen-config'), {
    'template-filename': 'folders.js'
});
const args = new Arguments(config, presetsFile);
const input = new Input();
const generate = new Generate();

(async () => {
    try {

        if (process.env.NODE_ENV !== 'test') {
            const { type, hash, noTemplate } = await args.parse();
            const templateFilepath = path.join(process.cwd(), noTemplate ? '.__BGCLI_TEMP_TEMPLATE_FILE__' : config.getOption('template-filename'));

            if (type) { // Ensures user have provided a valid argument
                if (type === 'rebuild' && !fs.existsSync(templateFilepath)) {
                    console.log('Could not detect a template file.');
                    process.exit(1);
                }

                if (['preset', 'direct', 'file'].includes(type)) {
                    if (fs.existsSync(templateFilepath)) {
                        if (await input.confirmOverwrite('the template file')) {
                            await generate.generateFile(templateFilepath, new Buffer(hash, 'base64'), { forceOverwrite: true });
                        } else {
                            process.exit(0);
                        }
                    } else {
                        await generate.generateFile(templateFilepath, new Buffer(hash, 'base64'));
                    }
                }

                const templateFile = require(templateFilepath);

                for (const [, folderPath] of (Object.entries(templateFile).filter(([key]) => key !== '_files'))) {
                    await generate.generateFolder(folderPath);
                }

                for (const { name, path: filePath, base64 } of templateFile._files) {
                    const file = path.join(filePath, name);

                    if (fs.existsSync(file)) {
                        if (await input.confirmOverwrite(path.relative(process.cwd(), file))) {
                            await generate.generateFile(file, new Buffer(base64, 'base64'), { forceOverwrite: true });
                        }
                    } else {
                        await generate.generateFile(file, new Buffer(base64, 'base64'));
                    }
                }

                if (noTemplate) {
                    fs.unlink(templateFilepath, err => {
                        if (err) throw err;
                    });
                }
            }
        }
    } catch (e) {
        console.log(e.stack);
    }
})();