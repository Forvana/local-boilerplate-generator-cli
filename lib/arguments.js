class Argument {
    constructor(config, presetsFile, { yargs, prompts, path, fs, json, input, generate } = {}) {
        this.yargs = yargs || require('yargs');
        this.prompts = prompts || require('prompts');
        this.path = path || require('path');
        this.fs = fs || require('fs');
        this.input = input || new (require('./prompts.js'))();
        this.generate = generate || new (require('./generation.js'))();
        this.JSON = json || JSON;
        this.config = config;
        this.presetsFile = presetsFile;
    }

    parse() {
        return new Promise(resolve => {
            this.yargs
                .strict()
                .command({
                    command: 'preset [id]',
                    aliases: ['presets'],
                    desc: 'Select a preset from a list in your global config',
                    builder: yargs => yargs
                        .option('n', {
                            alias: ['noTemplate', 'notemplate'],
                            desc: 'Build a preset without generating a template file',
                            type: 'boolean',
                            default: false
                        }),
                    handler: argv => this.presetAction(resolve, argv)
                })
                .command({
                    command: 'build',
                    desc: 'Generate a boilerplate using a base64 hash or a file containing a base64 hash',
                    builder: yargs => yargs
                        .options({
                            'H': {
                                alias: 'hash',
                                desc: 'Directly pass in a hash'
                            },
                            'f': {
                                alias: 'file',
                                desc: 'Get the hash from a file'
                            }
                        })
                        .conflicts('hash', 'file')
                        .conflicts('file', 'hash'),
                    handler: argv => {
                        if (argv.hash || argv.file) {
                            this.buildAction(resolve, argv);
                        } else {
                            this.yargs.showHelp();
                            console.log('Please supply an option for the build command');
                        }
                    }
                })
                .command({
                    command: 'template',
                    desc: 'Display the template\'s preset hash',
                    handler: () => {
                        const configPath = this.path.join(process.cwd(), this.config.getOption('template-filename'));

                        console.log(this.generate.generatePresetString(this.fs.readFileSync(configPath)));
                    }
                })
                .command({
                    command: 'rebuild',
                    desc: 'Rebuild your boilerplate using your template file',
                    handler: () => this.rebuildAction(resolve)
                })
                .demandCommand(1, 'Must specify one type of action/command')
                .argv;
        });
    }

    async presetAction(resolve, { id, noTemplate }) {
        try {
            const presets = Object.keys(this.config.getOption('presets')).length
                ? this.config.getOption('presets')
                : this.JSON.parse(this.fs.readFileSync(this.presetsFile, 'utf8'));

            if (id > Object.entries(presets).length) {
                id = Object.entries(presets).length;
            }

            if (id <= 0) {
                id = 1;
            }

            resolve({
                type: 'preset',
                hash: id ? Object.entries(presets)[id - 1][1] : (await this.input.selectPreset(presets))[1],
                noTemplate
            });
        } catch (e) {
            console.log(e.stack);
        }
    }

    async buildAction(resolve, { hash, file: filepath }) {
        if (hash) {
            resolve({ type: 'direct', hash });
        } else if (filepath) {
            try {
                resolve({
                    type: 'file',
                    hash: this.fs.readFileSync(filepath, 'utf8')
                });
            } catch (e) {
                console.log(e.stack);
            }
        }
    }

    rebuildAction(resolve) {
        resolve({ type: 'rebuild' });
    }
}

module.exports = Argument;