class Configuration {
    constructor(filepath, options, { jsonfile, fs } = {}) {
        this.jsonfile = jsonfile || require('jsonfile');
        this.fs = fs || require('fs');
        this.filepath = filepath;
        this.options = { ...options, presets: {} };

        if (!this.fs.existsSync(this.filepath) && process.env.NODE_ENV !== 'test') {
            for (const [name, defaultValue] of Object.entries(this.options)) {
                this.createOption(name, defaultValue);
            }

            this.updateConfigurationFile();
        }
    }

    createOption(name, defaultValue) {
        this.options = { ...this.options, [name]: defaultValue };
    }

    updateConfigurationFile() {
        this.jsonfile.writeFileSync(this.filepath, this.options, { spaces: 2 }, err => {
            if (err) throw err;
        });
    }

    getOption(key) {
        const config = this.jsonfile.readFileSync(this.filepath);

        return config[key];
    }
}

module.exports = Configuration;