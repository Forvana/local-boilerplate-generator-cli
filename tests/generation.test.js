/* eslint-disable no-undef */
const Generation = require('../lib/generation.js');

describe('generation.js', () => {
    describe('generateFile', () => {
        it('generates a file if it does not already exist', () => {
            const fakeFs = {
                existsSync: jest.fn().mockReturnValue(false),
                writeFile: jest.fn()
            };

            const generate = new Generation({ fs: fakeFs });
            generate.generateFile('filename', 'content', { forceOverwrite: false });

            const writeFileCalls = fakeFs.writeFile.mock.calls[0];
            expect(fakeFs.writeFile).toBeCalled();
            expect(writeFileCalls[0]).toBe('filename');
            expect(writeFileCalls[1]).toBe('content');
        });

        it('overwrites an existing file', () => {
            const fakeFs = {
                existsSync: jest.fn().mockReturnValue(true),
                writeFile: jest.fn()
            };

            const generate = new Generation({ fs: fakeFs });
            generate.generateFile('filename', 'content', { forceOverwrite: true });

            const writeFileCalls = fakeFs.writeFile.mock.calls[0];
            expect(fakeFs.writeFile).toBeCalled();
            expect(writeFileCalls[0]).toBe('filename');
            expect(writeFileCalls[1]).toBe('content');
        });

        it('attempt to generate a file without overwriting existing', () => {
            const fakeFs = {
                existsSync: jest.fn().mockReturnValue(true),
                writeFile: jest.fn()
            };

            const generate = new Generation({ fs: fakeFs });
            generate.generateFile('filename', 'content', { forceOverwrite: false });

            expect(fakeFs.writeFile).not.toBeCalled();
        });
    });
});