/* eslint-disable no-undef */
const Argument = require('../lib/arguments.js');

describe('arguments.js', () => {
    describe('presetsAction', () => {
        describe('preset id not provided', () => {
            it('loads presets from config file', () => {
                const fakeConfig = {
                    getOption: jest.fn().mockReturnValue({
                        'Preset #1': '#',
                        'Preset #2': '##',
                        'Preset #3': '###'
                    })
                };

                const fakeInput = {
                    selectPreset: jest.fn().mockReturnValue(['filter', 'data'])
                };

                const args = new Argument(fakeConfig, null, { input: fakeInput });
                args.presetAction(jest.fn(), {});

                expect(fakeInput.selectPreset).toBeCalled();
                expect(fakeConfig.getOption.mock.calls.map(call => call[0])).toEqual(['presets', 'presets']);
                expect(fakeConfig.getOption.mock.calls.length).toBe(2);
            });

            it('loads presets from old presets file', () => {
                const fakeConfig = {
                    getOption: jest.fn().mockReturnValue({})
                };

                const fakeJson = {
                    parse: jest.fn().mockReturnValue({
                        'Preset #1': '#',
                        'Preset #2': '##',
                        'Preset #3': '###'
                    })
                };

                const fakeFs = {
                    readFileSync: jest.fn()
                };

                const fakeInput = {
                    selectPreset: jest.fn().mockReturnValue(['filter', 'data'])
                };

                const args = new Argument(fakeConfig, 'presets', {
                    input: fakeInput,
                    json: fakeJson,
                    fs: fakeFs
                });

                args.presetAction(jest.fn(), {});

                expect(fakeInput.selectPreset).toBeCalled();
                expect(fakeJson.parse).toBeCalled();
                expect(fakeFs.readFileSync).toBeCalled();
                expect(fakeFs.readFileSync.mock.calls[0][0]).toBe('presets');
            });
        });

        describe('preset id provided', () => {
            let args;

            beforeEach(() => {
                const fakeConfig = {
                    getOption: jest.fn().mockReturnValue({
                        'Preset #1': '#',
                        'Preset #2': '##',
                        'Preset #3': '###'
                    })
                };

                args = new Argument(fakeConfig, null);
            });

            it('converts id from zero-based to ?one-based?', async () => {
                const result = await new Promise(resolve => {
                    args.presetAction(resolve, { id: 2 });
                });

                expect(result).toEqual({
                    type: 'preset',
                    hash: '##'
                });
            });

            it('maxs the id', async () => {
                const result = await new Promise(resolve => {
                    args.presetAction(resolve, { id: 4 });
                });

                expect(result).toEqual({
                    type: 'preset',
                    hash: '###'
                });
            });

            it('mins the id', async () => {
                const result = await new Promise(resolve => {
                    args.presetAction(resolve, { id: 0 });
                });

                expect(result).toEqual({
                    type: 'preset',
                    hash: '#'
                });
            });
        });

        it('returns noTemplate', async () => {
            const fakeConfig = {
                getOption: jest.fn().mockReturnValue({
                    'Preset #1': '#',
                    'Preset #2': '##',
                    'Preset #3': '###'
                })
            };

            const args = new Argument(fakeConfig, null);

            const result = await new Promise(resolve => {
                args.presetAction(resolve, { id: 2, noTemplate: true });
            });

            expect(result).toEqual({
                type: 'preset',
                noTemplate: true,
                hash: '##'
            });
        });
    });

    describe('buildAction', () => {
        describe('direct', () => {
            it('returns the payload', async () => {
                const args = new Argument(null, null, {});

                const result = await new Promise(resolve => {
                    args.buildAction(resolve, { hash: '##' });
                });

                expect(result).toEqual({
                    type: 'direct',
                    hash: '##'
                });
            });
        });

        describe('file', () => {
            it('returns the payload', async () => {
                const fakeFs = {
                    readFileSync: jest.fn().mockReturnValue('##')
                };

                const args = new Argument(null, null, { fs: fakeFs });

                const result = await new Promise(resolve => {
                    args.buildAction(resolve, { file: 'path/to/file' });
                });

                expect(fakeFs.readFileSync).toBeCalled();
                expect(fakeFs.readFileSync.mock.calls[0][0]).toBe('path/to/file');
                expect(result).toEqual({
                    type: 'file',
                    hash: '##'
                });
            });
        });
    });

    describe('rebuildAction', () => {
        it('returns the payload', async () => {
            const args = new Argument();

            const result = await new Promise(resolve => {
                args.rebuildAction(resolve);
            });

            expect(result).toEqual({
                type: 'rebuild'
            });
        });
    });
});