class Generate {
    constructor({ fs, nozip } = {}) {
        this.fs = fs || require('fs');
        this.nozip = nozip || require('node-zip');
    }

    generateFile(filepath, stream, { forceOverwrite = false } = {}) {
        return new Promise((resolve, reject) => {
            if (forceOverwrite || !this.fs.existsSync(filepath)) {
                this.fs.writeFile(filepath, stream, err => {
                    err ? reject(err) : resolve();
                });
            } else if (this.fs.existsSync(filepath)) {
                resolve();
            }
        });
    }

    generateFolder(folderpath) {
        return new Promise((resolve, reject) => {
            if (!this.fs.existsSync(folderpath)) {
                this.fs.mkdir(folderpath, err => {
                    err && reject(err);
                });
            }

            resolve();
        });
    }

    generatePresetString(content) {
        const minify = new this.nozip();

        minify.file('', content);
        return minify.generate({ base64: true, compression: 'deflate' });
    }

    uncompressPresetString(data) {
        const extract = new this.nozip(data, { base64: true, checkCRC32: true });

        return extract.files['']._data;
    }
}

module.exports = Generate;