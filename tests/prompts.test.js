/* eslint-disable no-undef */
const Input = require('../lib/prompts.js');

describe('prompts.js', () => {
    describe('selectPreset', () => {
        it('gave prompts an array of choice objects', async () => {
            const prompts = jest.fn().mockReturnValue({});
            const presets = {
                'Preset #1': '#',
                'Preset #2': '##',
                'Preset #3': '###'
            };

            const input = new Input({ prompts });

            await input.selectPreset(presets);

            expect(prompts.mock.calls[0][0].choices).toEqual([
                {
                    'title': '1. Preset #1',
                    'value': [
                        'Preset #1',
                        '#'
                    ]
                },
                {
                    'title': '2. Preset #2',
                    'value': [
                        'Preset #2',
                        '##'
                    ]
                },
                {
                    'title': '3. Preset #3',
                    'value': [
                        'Preset #3',
                        '###'
                    ]
                }
            ]);
        });

        it('returns the correct preset', async () => {
            const prompts = jest.fn().mockReturnValue({ preset: ['Preset #2', '##'] });
            const presets = {
                'Preset #1': '#',
                'Preset #2': '##',
                'Preset #3': '###'
            };

            const input = new Input({ prompts });

            const result = await input.selectPreset(presets);

            return expect(result).toEqual(['Preset #2', '##']);
        });
    });

    describe('confirmOverwrite', () => {
        it('returns false', async () => {
            const prompts = jest.fn().mockReturnValue({
                confirmation: false
            });

            const input = new Input({ prompts });

            const result = await input.confirmOverwrite(null);

            return expect(result).toBe(false);
        });

        it('returns true', async () => {
            const prompts = jest.fn().mockReturnValue({
                confirmation: true
            });

            const input = new Input({ prompts });

            const result = await input.confirmOverwrite(null);

            return expect(result).toBe(true);
        });
    });
});